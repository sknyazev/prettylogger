import unittest

from src.parser.Lang.JSON import findBlocks, prettyJson, findAndFormat


class TestParser(unittest.TestCase):

    def test_basic(self):
        example_1 = '''
      hello world
      {"menu": {
        "id": "file",
        "value": "File",
        "popup": {
          "menuitem": [
            {"value": "New", "onclick": "CreateNewDoc()"},
            {"value": "Open", "onclick": "OpenDoc()"},
            {"value": "Close", "onclick": "CloseDoc()"}
          ]
        }
      }}'''

        self.assertEqual(1, len(findBlocks(example_1)))

    def test_get_real_block(self):
        example_1 = '''
              hello world
              {"menu": {
                "id": "file",
                "value": "File",
                "popup": {
                  "menuitem": [
                    {"value": "New", "onclick": "CreateNewDoc()"},
                    {"value": "Open", "onclick": "OpenDoc()"},
                    {"value": "Close", "onclick": "CloseDoc()"}
                  ]
                }
              }}'''

        real_block = '''{"menu": {
                "id": "file",
                "value": "File",
                "popup": {
                  "menuitem": [
                    {"value": "New", "onclick": "CreateNewDoc()"},
                    {"value": "Open", "onclick": "OpenDoc()"},
                    {"value": "Close", "onclick": "CloseDoc()"}
                  ]
                }
              }}'''
        resultList = findBlocks(example_1)
        result = resultList[0]
        self.assertEqual(real_block, result)


    def test_pretty(self):
        before = '''
          {"menu": {"id": "file","value": "File",
            "popup": {
              "menuitem": [
                {"value": "New", "onclick": "CreateNewDoc()"},
                {"value": "Open", "onclick": "OpenDoc()"},
                {"value": "Close", "onclick": "CloseDoc()"}
              ]
            }
          }}'''

        after = '''{
    "menu": {
        "id": "file",
        "popup": {
            "menuitem": [
                {
                    "onclick": "CreateNewDoc()",
                    "value": "New"
                },
                {
                    "onclick": "OpenDoc()",
                    "value": "Open"
                },
                {
                    "onclick": "CloseDoc()",
                    "value": "Close"
                }
            ]
        },
        "value": "File"
    }
}'''
        result = prettyJson(before)
        self.assertEqual(after, result)

    def test_pretty_all_json_blocks(self):
        before = '''hello world
{"menu": {"id": "file","value": "File",
            "popup": {
              "menuitem": [
                {"value": "New", "onclick": "CreateNewDoc()"},
                {"value": "Open", "onclick": "OpenDoc()"},
                {"value": "Close", "onclick": "CloseDoc()"}
              ]
            }
          }}'''

        after = '''hello world
{
    "menu": {
        "id": "file",
        "popup": {
            "menuitem": [
                {
                    "onclick": "CreateNewDoc()",
                    "value": "New"
                },
                {
                    "onclick": "OpenDoc()",
                    "value": "Open"
                },
                {
                    "onclick": "CloseDoc()",
                    "value": "Close"
                }
            ]
        },
        "value": "File"
    }
}'''
        result = findAndFormat(before)
        self.assertEqual(after, result)

if __name__ == '__main__':
    unittest.main()
