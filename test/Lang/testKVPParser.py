import unittest

from src.parser.Lang.KVP import findBlocks, prettyKVP, findAndFormat


class TestParser(unittest.TestCase):

    def test_basic(self):
        example_1 = '''hello world
authentication.userId=8a8294174b7ecb28014b9699220015cc
authentication.password=sy6KJsT8
authentication.entityId=8a8294174b7ecb28014b9699220015ca
amount=92.00
currency=EUR
paymentBrand=VISA
paymentType=DB
card.number=4200000000000000
card.holder=Jane Jones
card.expiryMonth=05
card.expiryYear=2020
card.cvv=123'''

        self.assertEqual(1, len(findBlocks(example_1)))

    def test_get_real_block(self):
        example_1 = '''hello world
authentication.userId=8a8294174b7ecb28014b9699220015cc
authentication.password=sy6KJsT8
authentication.entityId=8a8294174b7ecb28014b9699220015ca
amount=92.00
currency=EUR
paymentBrand=VISA
paymentType=DB
card.number=4200000000000000
card.holder=Jane Jones
card.expiryMonth=05
card.expiryYear=2020
card.cvv=123'''

        real_block = '''authentication.userId=8a8294174b7ecb28014b9699220015cc
authentication.password=sy6KJsT8
authentication.entityId=8a8294174b7ecb28014b9699220015ca
amount=92.00
currency=EUR
paymentBrand=VISA
paymentType=DB
card.number=4200000000000000
card.holder=Jane Jones
card.expiryMonth=05
card.expiryYear=2020
card.cvv=123'''
        resultList = findBlocks(example_1)
        result = resultList[0]
        self.assertEqual(real_block, result)


    def test_pretty(self):
        before = '''
          authentication.userId=8a8294174b7ecb28014b9699220015cc
authentication.password=sy6KJsT8
authentication.entityId=8a8294174b7ecb28014b9699220015ca
    amount=92.00
currency=EUR
        paymentBrand=VISA
paymentType=DB
        card.number=4200000000000000
card.holder=Jane Jones
card.expiryMonth=05
card.expiryYear=2020
card.cvv=123'''

        after = '''authentication.userId=8a8294174b7ecb28014b9699220015cc
authentication.password=sy6KJsT8
authentication.entityId=8a8294174b7ecb28014b9699220015ca
amount=92.00
currency=EUR
paymentBrand=VISA
paymentType=DB
card.number=4200000000000000
card.holder=Jane Jones
card.expiryMonth=05
card.expiryYear=2020
card.cvv=123'''
        result = prettyKVP(before)
        self.assertEqual(after, result)

    def test_pretty_all_kvp_blocks(self):
        before = '''hello world
    authentication.userId=8a8294174b7ecb28014b9699220015cc
authentication.password=sy6KJsT8
authentication.entityId=8a8294174b7ecb28014b9699220015ca
    amount=92.00
currency=EUR
        paymentBrand=VISA
paymentType=DB
        card.number=4200000000000000
card.holder=Jane Jones
card.expiryMonth=05
card.expiryYear=2020
card.cvv=123'''

        after = '''hello world
authentication.userId=8a8294174b7ecb28014b9699220015cc
authentication.password=sy6KJsT8
authentication.entityId=8a8294174b7ecb28014b9699220015ca
amount=92.00
currency=EUR
paymentBrand=VISA
paymentType=DB
card.number=4200000000000000
card.holder=Jane Jones
card.expiryMonth=05
card.expiryYear=2020
card.cvv=123'''
        result = findAndFormat(before)
        self.assertEqual(after, result)

if __name__ == '__main__':
    unittest.main()
