import unittest

from src.parser.Lang.XML import findBlocks, prettyXML, findAndFormat


class TestParser(unittest.TestCase):

    def test_basic(self):
        example_1 = '''
        hello world
<slideshow><title>Demo slideshow</title>
<slide><title>Slide title</title>
<point>This is a demo</point>
<point>Of a program for processing slides</point>
</slide><slide><title>Another demo slide</title>
<point>It is important</point>
<point>To have more than</point>
<point>one slide</point>
</slide>
</slideshow>'''

        self.assertEqual(1, len(findBlocks(example_1)))

    def test_get_real_block(self):
        example_1 = '''
              hello world
<slideshow>
<title>Demo slideshow</title>
<slide><title>Slide title</title>
<point>This is a demo</point>
<point>Of a program for processing slides</point>
</slide>

<slide><title>Another demo slide</title>
<point>It is important</point>
<point>To have more than</point>
<point>one slide</point>
</slide>
</slideshow>'''

        real_block = '''<slideshow>
<title>Demo slideshow</title>
<slide><title>Slide title</title>
<point>This is a demo</point>
<point>Of a program for processing slides</point>
</slide>

<slide><title>Another demo slide</title>
<point>It is important</point>
<point>To have more than</point>
<point>one slide</point>
</slide>
</slideshow>'''
        resultList = findBlocks(example_1)
        result = resultList[0]
        self.assertEqual(real_block, result)


    def test_pretty(self):
        before = '''<slideshow>
<title>Demo slideshow</title>
<slide><title>Slide title</title>
<point>This is a demo</point>
<point>Of a program for processing slides</point>
</slide>

<slide><title>Another demo slide</title>
<point>It is important</point>
<point>To have more than</point>
<point>one slide</point>
</slide>
</slideshow>'''

        after = '''<slideshow>
  <title>Demo slideshow</title>
  <slide>
    <title>Slide title</title>
    <point>This is a demo</point>
    <point>Of a program for processing slides</point>
  </slide>
  <slide>
    <title>Another demo slide</title>
    <point>It is important</point>
    <point>To have more than</point>
    <point>one slide</point>
  </slide>
</slideshow>'''
        result = prettyXML(before)
        self.assertEqual(after, result)

    def test_pretty_all_json_blocks(self):
        before = '''hello world
<slideshow><title>Demo slideshow</title>
<slide><title>Slide title</title>
<point>This is a demo</point>
<point>Of a program for processing slides</point>
</slide><slide><title>Another demo slide</title>
<point>It is important</point>
<point>To have more than</point>
<point>one slide</point>
</slide>
</slideshow>'''

        after = '''hello world
<slideshow>
  <title>Demo slideshow</title>
  <slide>
    <title>Slide title</title>
    <point>This is a demo</point>
    <point>Of a program for processing slides</point>
  </slide>
  <slide>
    <title>Another demo slide</title>
    <point>It is important</point>
    <point>To have more than</point>
    <point>one slide</point>
  </slide>
</slideshow>'''
        result = findAndFormat(before)
        self.assertEqual(after, result)

if __name__ == '__main__':
    unittest.main()
