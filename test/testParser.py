import unittest

from src.parser.Parser import prettyJSON_XML


class TestParser(unittest.TestCase):

    def test_first(self):
        example = '''hello world
{"menu": {
        "id": "file",
        "value": "File",
        "popup": {
          "menuitem": [
            {"value": "New", "onclick": "CreateNewDoc()"},
            {"value": "Open", "onclick": "OpenDoc()"},
            {"value": "Close", "onclick": "CloseDoc()"}
          ]
        }
      }}
2014-07-02 20:52:39 DEBUG HelloExample:19 - This is debug : mkyong
2014-07-02 20:52:39 INFO  HelloExample:23 - This is info : mkyong
2014-07-02 20:52:39 WARN  HelloExample:26 - This is warn : mkyong
hello world
    authentication.userId=8a8294174b7ecb28014b9699220015cc
authentication.password=sy6KJsT8
authentication.entityId=8a8294174b7ecb28014b9699220015ca
    amount=92.00
currency=EUR
        paymentBrand=VISA
paymentType=DB
        card.number=4200000000000000
card.holder=Jane Jones
card.expiryMonth=05
card.expiryYear=2020
card.cvv=123
2014-07-02 20:52:39 DEBUG HelloExample:19 - This is debug : mkyong
2014-07-02 20:52:39 INFO  HelloExample:23 - This is info : mkyong
2014-07-02 20:52:39 WARN  HelloExample:26 - This is warn : mkyong
hello world
    authentication.userId=sd32e32ve32ev23
authentication.password=v234ev32ev23v
authentication.entityId=v23e32v32vr32rv23
        amount=91.00
currency=EUR
paymentBrand=VISA
paymentType=DB
   card.number=4200000000000000
card.holder=Jane Jones
card.expiryMonth=05
card.expiryYear=2020
card.cvv=143
2014-07-02 20:52:39 ERROR HelloExample:27 - This is error : mkyong
2014-07-02 20:52:39 FATAL HelloExample:28 - This is fatal : mkyong
<font color="#069"><person></font><font color="#069"><firstname></font>Subbu<font color="#069"></firstname></font>
      <font color="#069"><lastname></font>Allamaraju<font color="#069"></lastname></font>
   <font color="#069"></person></font>'''

        after = '''hello world
{
    "menu": {
        "id": "file",
        "popup": {
            "menuitem": [
                {
                    "onclick": "CreateNewDoc()",
                    "value": "New"
                },
                {
                    "onclick": "OpenDoc()",
                    "value": "Open"
                },
                {
                    "onclick": "CloseDoc()",
                    "value": "Close"
                }
            ]
        },
        "value": "File"
    }
}
2014-07-02 20:52:39 DEBUG HelloExample:19 - This is debug : mkyong
2014-07-02 20:52:39 INFO  HelloExample:23 - This is info : mkyong
2014-07-02 20:52:39 WARN  HelloExample:26 - This is warn : mkyong
hello world
authentication.userId=8a8294174b7ecb28014b9699220015cc
authentication.password=sy6KJsT8
authentication.entityId=8a8294174b7ecb28014b9699220015ca
amount=92.00
currency=EUR
paymentBrand=VISA
paymentType=DB
card.number=4200000000000000
card.holder=Jane Jones
card.expiryMonth=05
card.expiryYear=2020
card.cvv=123
2014-07-02 20:52:39 DEBUG HelloExample:19 - This is debug : mkyong
2014-07-02 20:52:39 INFO  HelloExample:23 - This is info : mkyong
2014-07-02 20:52:39 WARN  HelloExample:26 - This is warn : mkyong
hello world
authentication.userId=sd32e32ve32ev23
authentication.password=v234ev32ev23v
authentication.entityId=v23e32v32vr32rv23
amount=91.00
currency=EUR
paymentBrand=VISA
paymentType=DB
card.number=4200000000000000
card.holder=Jane Jones
card.expiryMonth=05
card.expiryYear=2020
card.cvv=143
2014-07-02 20:52:39 ERROR HelloExample:27 - This is error : mkyong
2014-07-02 20:52:39 FATAL HelloExample:28 - This is fatal : mkyong
<font color="#069">
  <person></font>
  <font color="#069"><firstname></font>Subbu<font color="#069"></firstname></font>
  <font color="#069"><lastname></font>Allamaraju<font color="#069"></lastname></font>
  <font color="#069"></person>
</font>'''
        resalt = prettyJSON_XML(example)
        self.assertEqual(after, resalt)


if __name__ == '__main__':
    unittest.main()
