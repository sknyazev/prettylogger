import re

from yattag import indent


def findBlocks(text):
    regexp = r"(<[\s\S]*>)"
    result = re.findall(regexp, text)
    return result


def prettyXML(text):
    # xmlDOM = xml.dom.minidom.parseString(text)
    # result = xmlDOM.toprettyxml()
    # return result

    pretty_string = indent(text)
    return pretty_string


def findAndFormat(text):
    blocks = findBlocks(text)
    for block in blocks:
        text = text.replace(block, prettyXML(block))
    return text
