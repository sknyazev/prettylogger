import re


def findBlocks(text):
    regexp = r"[\t ]*[a-zA-Z0-9.]+(:|=)[a-zA-Z0-9.]+[\t ]*"
    result = []

    prev = True
    block = ""
    for line in text.splitlines():
        local_result = re.findall(regexp, line)
        if len(local_result) > 1 or len(local_result) == 0:
            if prev:
                block = block[:-1]
                if block:
                    result.append(block[:-1])
                block = ""
            prev = False
        else:
            prev = True
            block += line + '\n'
    if block.strip():
        result.append(block[:-1])
    return result


def prettyKVP(KVPBlock):
    KVPlist = parseKVP(KVPBlock)
    KVPNewBlock = buildKVP(KVPlist)
    #text = KVPBlock.replace(KVPBlock, KVPNewBlock)
    return KVPNewBlock


def parseKVP(text):
    result = []
    for line in text.splitlines():
        line = line.strip()
        if line:
            line_lst = line.split("=")
            sep = "="
            if len(line_lst) != 2:
                line_lst = line.split(":")
                sep = ":"
            if len(line_lst) != 2:
                continue
            name, value = line_lst[0], line_lst[1]
            result.append({'name': name.strip(), 'sep': sep.strip(), 'value': value.strip()})
    return result


def buildKVP(KVPlist):
    result = ""
    for item in KVPlist:
        name, sep, value = item.get('name'), item.get('sep'), item.get('value')
        result += "{}{}{}\n".format(name, sep, value)

    return result[:-1]


def findAndFormat(text):
    blocks = findBlocks(text)

    for block in blocks:
        '''
        print("Start block")
        print(block)
        print("End block\n\n")
        prettyKVPBLock = prettyKVP(block)
        print("Start block")
        print(prettyKVPBLock)
        print("End block\n\n")
        '''
        prettyKVPBLock = prettyKVP(block)
        text = text.replace(block, prettyKVPBLock)
    return text
