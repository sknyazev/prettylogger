import json
import re


def findBlocks(text):
    regexp = r"(\{[\s\S]+:[\s\S]+[\s\S]*\})"
    result = re.findall(regexp, text)
    return result

def prettyJson(text):
    json_data = json.loads(text)
    #result = pprint.pformat(json_data)
    result = json.dumps(json_data, indent=4, sort_keys=True)
    return result

def findAndFormat(text):
    blocks = findBlocks(text)
    for block in blocks:
        text = text.replace(block, prettyJson(block))
    return text
