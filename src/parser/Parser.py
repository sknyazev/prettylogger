from src.parser.Lang.JSON import findAndFormat as JSONff
from src.parser.Lang.KVP import findAndFormat as KVPff
from src.parser.Lang.XML import findAndFormat as XMLff


def prettyJSON_XML(text):
    text = JSONff(text)
    text = XMLff(text)
    text = KVPff(text)
    return text
